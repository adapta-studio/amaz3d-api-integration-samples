import json
import time
import requests
import logging
import warnings
from requests.packages.urllib3.exceptions import InsecureRequestWarning

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logging.getLogger('urllib3').setLevel(logging.INFO)

warnings.simplefilter('ignore', InsecureRequestWarning)

graphql_endpoint = "https://amaz3d_backend.adapta.studio/graphql"
username = "username"
password = "password"
path_object_model_1 = "/path/to/file"
project_name_1 = "MyProjectName"
optimization_name = "MyOptimizatioName"

def login(email, password):
    query = """
    mutation Login($input: LoginInput!) {
        login(input: $input){
            token
            refreshToken
        }
    }
    """
    variables = {
        "input": {
            "email": email,
            "password": password
        }
    }
    response = requests.post(graphql_endpoint, json={'query': query, 'variables': variables}, verify=False)
    response.raise_for_status()
    return response.json()['data']['login']['token']

def upload_model(token, model_path):
    
    operations = {
        "operationName": "UploadProjectFile",
        "variables": {
            "input": {
                "file": None,
                "relatedTo": None
            }
        },
        "query": """mutation UploadProjectFile($input: FileUploadInput!) {
            uploadObjectModel(input: $input) {
                id
                __typename
            }
        }"""
    }
    
    map_data = {
        "1": ["variables.input.file"]
    }
    
    headers = {
        "Authorization": f"Bearer {token}"
    }
    
    files = {
        'operations': (None, json.dumps(operations), 'application/json'),
        'map': (None, json.dumps(map_data), 'application/json'),
        '1': (model_path, open(model_path, 'rb'), 'application/octet-stream')
    }
    
    response = requests.post(graphql_endpoint, headers=headers, files=files, verify=False)
    
    if response.status_code == 200:
        model_id = response.json()['data']['uploadObjectModel']['id']
        return model_id
    else:
        response.raise_for_status()

def create_project(token, project_name, model_id):
    
    query = """
    mutation CreateProject($input: CreateOneProjectInput!) {
      createOneProject(input: $input) {
        conversionStatus
        allowsNormalsBaking
        allowsVertexMaskPolygonReduction
        lastActivityAt
        id
        name
        objectModel {
          fileSizeBytes
          picture {
            publicUrl
          }
          triangleCount
          name
        }
      }
    }
    """
    
    variables = {
        "input": {
            "project": {
                "name": project_name,
                "objectModel": {
                    "id": model_id
                },
            }
        }
    }
    
    payload = {
        "query": query,
        "variables": variables
    }
    
    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json"
    }
    
    response = requests.post(graphql_endpoint, headers=headers, json=payload, verify=False)
    
    if response.status_code == 200:
        return response.json()['data']['createOneProject']['id']
    else:
        print(f"An error occurred: {response.status_code}")
        print(f"Response content: {response.content.decode()}")
        response.raise_for_status()

def create_optimization(token, optimization_name, project_id):

    query = """
    mutation CreateOptimization($input: CreateOneOptimizationInput!) {
      createOneOptimization(input: $input) {
        id
        name
        status
        outputFormat
        outputFormatOptions {
          format_glb {
            export_draco_mesh_compression_enable
            __typename
          }
          format_gltf {
            export_draco_mesh_compression_enable
            export_format
            __typename
          }
          __typename
        }
        type
        relatedTo {
          id
          name
          __typename
        }
        sharing {
          id
          sharingUrl
          __typename
        }
        info {
          version
          images {
            width
            height
            bitdepth
            path
            ext
            __typename
          }
          __typename
        }
        nbparams {
          opacity
          width
          height
          image_format
          normal_format
          __typename
        }
        params {
          face_reduction
          feature_importance
          feature_area
          uv_seam_importance
          preserve_boundary_edges
          preserve_hard_edges
          preserve_smooth_edges
          retexture
          merge_duplicated_uv
          merge_gltf_primitives
          join_meshes
          remove_isolated_vertices
          remove_non_manifold_faces
          remove_duplicated_faces
          remove_duplicated_boundary_vertices
          remove_degenerate_faces
          remove_hidden_objects
          project_normals
          use_vertex_mask
          resize_images
          normals_weighting
          contrast
          joined_simplification
          normals_scaling
          minimum_face_number
          remove_meshes_by_size
          remove_meshes_by_count
          __typename
        }
        preset
        objectModelResult {
          id
          name
          path
          picture {
            publicUrl
            __typename
          }
          publicUrl
          triangleCount
          fileSizeBytes
          additionals {
            name
            publicUrl2
            publicUrl
            id
            path
            __typename
          }
          __typename
        }
        objectModelResultConverted {
          id
          name
          path
          picture {
            publicUrl
            __typename
          }
          publicUrl2
          publicUrl
          triangleCount
          fileSizeBytes
          additionals {
            name
            publicUrl2
            publicUrl
            id
            path
            __typename
          }
          __typename
        }
        lastActivityAt
        feedback
        objectModelResultObj {
          id
          name
          path
          picture {
            publicUrl
            __typename
          }
          publicUrl2
          publicUrl
          triangleCount
          fileSizeBytes
          __typename
        }
        project {
          id
          __typename
        }
        __typename
      }
    }
    """
    variables = {
        "input": {
            "optimization": {
                "name": optimization_name,
                "outputFormat": "format_orig",
                "outputFormatOptions": {},
                "preset": None,
                "params": {
                    "face_reduction": 0.5,
                    "feature_importance": 0,
                    "feature_area": 0.5,
                    "uv_seam_importance": 1,
                    "preserve_boundary_edges": 1,
                    "textures_to_jpeg": False,
                    "preserve_hard_edges": True,
                    "preserve_smooth_edges": True,
                    "retexture": True,
                    "merge_duplicated_uv": True,
                    "merge_gltf_primitives": False,
                    "join_meshes": False,
                    "remove_isolated_vertices": True,
                    "remove_non_manifold_faces": False,
                    "remove_duplicated_faces": True,
                    "remove_duplicated_boundary_vertices": True,
                    "remove_degenerate_faces": True,
                    "remove_hidden_objects": False,
                    "project_normals": False,
                    "use_vertex_mask": False,
                    "resize_images": 0,
                    "normals_weighting": 1,
                    "contrast": 0.5,
                    "joined_simplification": True,
                    "normals_scaling": 0,
                    "remove_meshes_by_size": 0,
                    "remove_meshes_by_count": 0,
                    "minimum_face_number": 0
                },
                "project": {
                    "id": project_id
                }
            }
        }
    }
    headers = {"Authorization": f"Bearer {token}", "Content-Type": "application/json"}
    response = requests.post(graphql_endpoint, json={'query': query, 'variables': variables}, headers=headers, verify=False)
    
    if response.status_code == 200:
        response_json = response.json()
        if 'errors' in response_json:
            print(f"Error: {response_json['errors'][0]['message']}")
            return None
        optimization_id = response_json['data']['createOneOptimization']['id']
        return optimization_id
    else:
        print(f"An error occurred: {response.status_code}")
        print(f"Response content: {response.content.decode()}")
        response.raise_for_status()

def get_optimization(token, optimization_id):
    
    query = """
    query Optimization($id: ID!) {
      optimization(id: $id) {
        status
        id
        name
        objectModelResult {
            fileSizeBytes
            triangleCount
            name
            publicUrl
        }
      }
    }
    """
    
    variables = {"id": optimization_id}
    
    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json"
    }
    
    response = requests.post(graphql_endpoint, json={'query': query, 'variables': variables}, headers=headers, verify=False)
    
    if response.status_code == 200:
        response_json = response.json()
        optimization_data = response_json['data']['optimization']
        status = optimization_data['status']
        return status, optimization_data
    else:
        print(f"An error occurred: {response.status_code}")
        print(f"Response content: {response.content.decode()}")
        response.raise_for_status()

try:
    token = login(username, password)
    print(f"Login successful. Token: {token}")

    model_id_1 = upload_model(token, path_object_model_1)
    print(f"Model uploaded. ID: {model_id_1}")

    project_id_1 = create_project(token, project_name_1, model_id_1)
    print(f"Project created. ID: {project_id_1}")

    optimization_id = create_optimization(token, optimization_name, project_id_1)

    if optimization_id is None:
        print("Optimization creation failed.")
    else:
        print(f"Optimization created. ID: {optimization_id}")

    while True:
        status, optimization_data = get_optimization(token, optimization_id)
        print(f"Optimization status obtained with polling: {status} (please avoid polling and use callback instead)")

        if status == "completed":
            public_url = optimization_data['objectModelResult']['publicUrl']
            print(f"Optimization completed. Result URL: {public_url}")
            break
        elif status == "error":
            print("Optimization encountered an error.")
            break

        time.sleep(5)

except requests.exceptions.RequestException as e:
    print(f"An error occurred: {e}")