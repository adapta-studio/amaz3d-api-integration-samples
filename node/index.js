const axios = require("axios");
const FormData = require("form-data");
const fs = require("fs");

const graphql_endpoint = "https://amaz3d_backend.adapta.studio/graphql"
const username = "username";
const password = "password";
const path_object_model_1 = "/path/to/file.glb";
const project_name_1 = "MyProjectName";
const optimization_name = "MyOptimizatioName";
const presigned_url = "https://mysignedurl.it/tests/file.glb?Signature=df10ce420d49bdc85168116a7ff628e7c063d66d596b7b0d02d78dcb38357f80"

async function login(email, password) {
  const query = `
    mutation Login($input: LoginInput!) {
        login(input: $input){
            token
            refreshToken
        }
    }
    `;
  const variables = {
    input: {
      email: email,
      password: password,
    },
  };
  const response = await axios.post(graphql_endpoint, {
    query: query,
    variables: variables,
  });
  return response.data.data.login.token;
}

async function upload_model(token, model_path) {
  const operations = {
    operationName: "UploadProjectFile",
    variables: {
      input: {
        file: null,
        relatedTo: null,
      },
    },
    query: `mutation UploadProjectFile($input: FileUploadInput!) {
            uploadObjectModel(input: $input) {
                id
                __typename
            }
        }`,
  };

  const map = {
    1: ["variables.input.file"],
  };

  const form = new FormData();
  form.append("operations", JSON.stringify(operations));
  form.append("map", JSON.stringify(map));
  form.append("1", fs.createReadStream(model_path));

  const headers = {
    ...form.getHeaders(),
    Authorization: `Bearer ${token}`,
  };

  const response = await axios.post(graphql_endpoint, form, {
    headers: headers,
  });

  return response.data.data.uploadObjectModel.id;
}

async function create_project(token, project_name, model_id) {
  const query = `
    mutation CreateProject($input: CreateOneProjectInput!) {
      createOneProject(input: $input) {
        conversionStatus
        allowsNormalsBaking
        allowsVertexMaskPolygonReduction
        lastActivityAt
        id
        name
        objectModel {
          fileSizeBytes
          picture {
            publicUrl
          }
          triangleCount
          name
        }
      }
    }
    `;

  const variables = {
    input: {
      project: {
        name: project_name,
        objectModel: {
          id: model_id,
        },
      },
    },
  };

  const headers = {
    Authorization: `Bearer ${token}`,
    "Content-Type": "application/json",
  };

  const response = await axios.post(
    graphql_endpoint,
    {
      query: query,
      variables: variables,
    },
    { headers: headers }
  );

  return response.data.data.createOneProject.id;
}

async function create_optimization(token, optimization_name, project_id, presigned_url) {
  const query = `
    mutation CreateOptimization($input: CreateOneOptimizationInput!) {
      createOneOptimization(input: $input) {
        id
        name
        status
        outputFormat
        outputFormatOptions {
          format_glb {
            export_draco_mesh_compression_enable
            __typename
          }
          format_gltf {
            export_draco_mesh_compression_enable
            export_format
            __typename
          }
          __typename
        }
        type
        relatedTo {
          id
          name
          __typename
        }
        sharing {
          id
          sharingUrl
          __typename
        }
        info {
          version
          images {
            width
            height
            bitdepth
            path
            ext
            __typename
          }
          __typename
        }
        nbparams {
          opacity
          width
          height
          image_format
          normal_format
          __typename
        }
        params {
          face_reduction
          feature_importance
          feature_area
          uv_seam_importance
          preserve_boundary_edges
          preserve_hard_edges
          preserve_smooth_edges
          retexture
          merge_duplicated_uv
          merge_gltf_primitives
          join_meshes
          remove_isolated_vertices
          remove_non_manifold_faces
          remove_duplicated_faces
          remove_duplicated_boundary_vertices
          remove_degenerate_faces
          remove_hidden_objects
          project_normals
          use_vertex_mask
          resize_images
          normals_weighting
          contrast
          joined_simplification
          normals_scaling
          minimum_face_number
          remove_meshes_by_size
          remove_meshes_by_count
          __typename
        }
        preset
        objectModelResult {
          id
          name
          path
          picture {
            publicUrl
            __typename
          }
          publicUrl
          triangleCount
          fileSizeBytes
          additionals {
            name
            publicUrl2
            publicUrl
            id
            path
            __typename
          }
          __typename
        }
        objectModelResultConverted {
          id
          name
          path
          picture {
            publicUrl
            __typename
          }
          publicUrl2
          publicUrl
          triangleCount
          fileSizeBytes
          additionals {
            name
            publicUrl2
            publicUrl
            id
            path
            __typename
          }
          __typename
        }
        lastActivityAt
        feedback
        objectModelResultObj {
          id
          name
          path
          picture {
            publicUrl
            __typename
          }
          publicUrl2
          publicUrl
          triangleCount
          fileSizeBytes
          __typename
        }
        project {
          id
          __typename
        }
        __typename
      }
    }
    `;

  const variables = {
    input: {
      optimization: {
        name: optimization_name,
        outputFormat: "format_orig",
        outputFormatOptions: {},
        preset: null,
        params: {
          face_reduction: 0.5,
          feature_importance: 0,
          feature_area: 0.5,
          uv_seam_importance: 1,
          preserve_boundary_edges: 1,
          textures_to_jpeg: false,
          preserve_hard_edges: true,
          preserve_smooth_edges: true,
          retexture: true,
          merge_duplicated_uv: true,
          merge_gltf_primitives: false,
          join_meshes: false,
          remove_isolated_vertices: true,
          remove_non_manifold_faces: false,
          remove_duplicated_faces: true,
          remove_duplicated_boundary_vertices: true,
          remove_degenerate_faces: true,
          remove_hidden_objects: false,
          project_normals: false,
          use_vertex_mask: false,
          resize_images: 0,
          normals_weighting: 1,
          contrast: 0.5,
          joined_simplification: true,
          normals_scaling: 0,
          remove_meshes_by_size: 0,
          remove_meshes_by_count: 0,
          minimum_face_number: 0,
        },
        publication: {
          mode: "presigned_url",
          target: "original_result",
          url: presigned_url
        },
        project: {
          id: project_id,
        },
      },
    },
  };

  const headers = {
    Authorization: `Bearer ${token}`,
    "Content-Type": "application/json",
  };

  const response = await axios.post(
    graphql_endpoint,
    {
      query: query,
      variables: variables,
    },
    { headers: headers }
  );

  return response.data.data.createOneOptimization.id;
}

async function get_optimization(token, optimization_id) {
  const query = `
    query Optimization($id: ID!) {
      optimization(id: $id) {
        status
        id
        name
        objectModelResult {
            fileSizeBytes
            triangleCount
            name
            publicUrl
        }
      }
    }
    `;

  const variables = { id: optimization_id };

  const headers = {
    Authorization: `Bearer ${token}`,
    "Content-Type": "application/json",
  };

  const response = await axios.post(
    graphql_endpoint,
    {
      query: query,
      variables: variables,
    },
    { headers: headers }
  );

  const optimization_data = response.data.data.optimization;
  return [optimization_data.status, optimization_data];
}

async function main() {
  try {
    const token = await login(username, password);
    console.log(`Login successful. Token: ${token}`);

    const model_id_1 = await upload_model(token, path_object_model_1);
    console.log(`Model 1 uploaded. ID: ${model_id_1}`);

    const project_id_1 = await create_project(
      token,
      project_name_1,
      model_id_1
    );
    console.log(`Project 1 created. ID: ${project_id_1}`);

    const optimization_id = await create_optimization(
      token,
      optimization_name,
      project_id_1,
      presigned_url
    );

    if (!optimization_id) {
      console.log("Optimization creation failed.");
    } else {
      console.log(`Optimization created. ID: ${optimization_id}`);

      while (true) {
        const [status, optimization_data] = await get_optimization(
          token,
          optimization_id
        );
        console.log(
          `Optimization status obtained with polling: ${status} (please avoid polling and use callback instead)`
        );

        if (status === "completed") {
          const public_url = optimization_data.objectModelResult.publicUrl;
          console.log(`Optimization completed. Result URL: ${public_url}`);
          break;
        } else if (status === "error") {
          console.log("Optimization encountered an error.");
          break;
        }

        await new Promise((resolve) => setTimeout(resolve, 5000));
      }
    }
  } catch (error) {
    console.error(`An error occurred: ${error}`);
  }
}

main();
